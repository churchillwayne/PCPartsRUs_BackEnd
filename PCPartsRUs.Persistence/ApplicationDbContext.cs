﻿using PCPartsRUs.Common.Models;
using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace PCPartsRUs.Persistence
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext()
            : base(@"Data Source=WAYNES-NOTEBOOK;Initial Catalog=ComputerParts;User ID=ComputerParts;Password=computerparts")
        {
        }

        public DbSet<Part> PartItems { get; set; }
        public DbSet<StockItem> StockItems { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
        }
        public override int SaveChanges()
        {
            AddAuditInfo();
            return base.SaveChanges();
        }

        private void AddAuditInfo()
        {

            foreach (var item in ChangeTracker.Entries())
            {
                if (item.Entity is BaseModel && (item.State == EntityState.Added || item.State == EntityState.Modified))
                {
                    if (item.State == EntityState.Added)
                        ((BaseModel)item.Entity).Created = DateTime.UtcNow;

                    ((BaseModel)item.Entity).Modified = DateTime.UtcNow;
                }
            }


            //var entries = ChangeTracker.Entries().Where(x => x.Entity is BaseModel && (x.State == EntityState.Added || x.State == EntityState.Modified));
            //foreach (var entry in entries)
            //{
            //    if (entry.State == EntityState.Added)
            //    {
            //        ((BaseModel)entry.Entity).Created = DateTime.UtcNow;
            //    }
            //((BaseModel)entry.Entity).Modified = DateTime.UtcNow;
            //}
        }

    }
}
