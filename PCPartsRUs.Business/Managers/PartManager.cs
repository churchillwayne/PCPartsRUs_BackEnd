﻿using AutoMapper;
using PCPartsRUs.Common.DTOs;
using PCPartsRUs.Common.Managers;
using PCPartsRUs.Common.UnitOfWork;

namespace PCPartsRUs.Business.Managers
{
    public class PartManager : IPartManager
    {
        private readonly IUnitOfWork _unitOfWork;

        public PartManager(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public PartDTO GetPartByID(int Id)
        {
            var part = _unitOfWork.Parts.GetById(Id);
            if (part == null)
                return null;
            return Mapper.Map<PartDTO>(part);
        }

    }
}
