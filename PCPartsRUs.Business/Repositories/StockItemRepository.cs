﻿using PCPartsRUs.Common.Models;
using PCPartsRUs.Common.Repositories;
using PCPartsRUs.Persistence;

namespace PCPartsRUs.Business.Repositories
{
    public class StockItemRepository : RepositoryBase<StockItem>, IStockItemRepository
    {
        public StockItemRepository(ApplicationDbContext context)
            : base(context)
        {
        }

    }
}
