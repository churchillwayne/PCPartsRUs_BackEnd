﻿using PCPartsRUs.Common.Models;
using PCPartsRUs.Common.Repositories;
using PCPartsRUs.Persistence;

namespace PCPartsRUs.Business.Repositories
{
    public class PartRepository : RepositoryBase<Part>, IPartRepository
    {
        public PartRepository(ApplicationDbContext context)
            : base(context)
        {
        }

    }
}
