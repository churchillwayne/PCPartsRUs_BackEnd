﻿using PCPartsRUs.Business.Repositories;
using PCPartsRUs.Common.Repositories;
using PCPartsRUs.Common.UnitOfWork;
using PCPartsRUs.Persistence;

namespace PCPartsRUs.Business.UOW
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _context;

        public IPartRepository Parts { get; set; }

        public IStockItemRepository Stock { get; set; }

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
            Parts = new PartRepository(_context);
            Stock = new StockItemRepository(_context);
        }

        public void Complete()
        {
            _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }

    }
}
