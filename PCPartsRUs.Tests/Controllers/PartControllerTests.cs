﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PCPartsRUs.Api.Controllers;
using PCPartsRUs.Common.DTOs;
using PCPartsRUs.Common.Managers;
using PCPartsRUs.Common.UnitOfWork;
using System.Web.Http.Results;

namespace PCPartsRUs.Tests.Controllers
{
    [TestClass]
    public class PartControllerTests
    {

        private PartController controller;
        private Mock<IPartManager> partManager;

        private const int ID_DOES_NOT_EXIST = 1;
        private const int ID_EXISTS = 2;

        [TestInitialize]
        public void TestInitialize()
        {
            var mockUOw = new Mock<IUnitOfWork>();
            partManager = new Mock<IPartManager>();

            // Set up some objects to be returned when the partmanager is called
            partManager.Setup(p => p.GetPartByID(ID_DOES_NOT_EXIST)).Returns<PartDTO>(null);
            partManager.Setup(p => p.GetPartByID(ID_EXISTS)).Returns(new PartDTO());

            controller = new PartController(mockUOw.Object, partManager.Object);
        }

        [TestMethod]
        public void Get_NoPartWithGivenIdExists_ShouldReturnNotFound()
        {
            var result = controller.Get(ID_DOES_NOT_EXIST);
            result.Should().BeOfType<NotFoundResult>();
        }

        [TestMethod]
        public void Get_PartWithGivenIdExists_ShouldReturnPartDTO()
        {
            var result = controller.Get(ID_EXISTS);
            result.Should().BeOfType<OkNegotiatedContentResult<PartDTO>>();
        }

    }
}
