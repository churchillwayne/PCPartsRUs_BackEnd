﻿using Microsoft.Web.Http;
using PCPartsRUs.Common.Managers;
using PCPartsRUs.Common.UnitOfWork;
using System.Web.Http;

namespace PCPartsRUs.Api.Controllers
{

    [ApiVersion("1.0")]
    [RoutePrefix("Parts/v{version:apiVersion}")]
    public class PartController : ApiController
    {
        private IUnitOfWork unitOfWork;
        private IPartManager manager;

        public PartController(IUnitOfWork unitOfWork, IPartManager manager)
        {
            this.unitOfWork = unitOfWork;
            this.manager = manager;
        }

        [Route("")]
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            var dto = manager.GetPartByID(id);
            if (dto == null)
                return NotFound();

            return Ok(dto);

        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }

    }
}
