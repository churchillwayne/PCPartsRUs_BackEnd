﻿using Microsoft.Web.Http.Routing;
using Ninject;
using PCPartsRUs.Api.App_Start;
using PCPartsRUs.Common.Mapping;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Routing;

namespace PCPartsRUs.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Ensure the server hosting the web site is in the list below
            var cors = new EnableCorsAttribute("http://localhost:57532", "*", "*");
            config.EnableCors(cors);

            // WebAPI Versioning
            // See https://github.com/Microsoft/aspnet-api-versioning/wiki/ for usage
            var constraintResolver = new DefaultInlineConstraintResolver()
            {
                ConstraintMap =  {
                    ["apiVersion"] = typeof( ApiVersionRouteConstraint )
                }
            };
            config.MapHttpAttributeRoutes(constraintResolver);
            config.AddApiVersioning();
            
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // Initialize AutoMapper
            AutoMapperConfig.InitializeMappingConfiguration();

        }
    }
}
