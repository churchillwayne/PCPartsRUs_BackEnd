﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PCPartsRUs.Common.Models
{
    public class Part : BaseModel
    {
        public Part()
        {
        }

        [Required]
        [StringLength(50)]
        public string Code { get; set; }

        [Required]
        [StringLength(200)]
        public string Description { get; set; }

        public IEnumerable<StockItem> StockItems { get; set; }
    }
}
