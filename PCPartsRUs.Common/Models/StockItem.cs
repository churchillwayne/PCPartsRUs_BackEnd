﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PCPartsRUs.Common.Models
{
    public class StockItem : BaseModel
    {
        public StockItem()
        {

        }

        [Required]
        [StringLength(50)]
        public string SerialNumber { get; set; }

        [Required]
        public int PartId { get; set; }

        [ForeignKey("PartId")]
        public virtual Part Part { get; set; }
    }
}
