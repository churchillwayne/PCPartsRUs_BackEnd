﻿using AutoMapper;
using PCPartsRUs.Common.DTOs;
using PCPartsRUs.Common.Models;

namespace PCPartsRUs.Common.Mapping
{
    public static class AutoMapperConfig
    {
        public static void InitializeMappingConfiguration()
        {
            // AutoMapper configuration to map from Entities to DTOs and Vice Versa
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Part, PartDTO>();
                cfg.CreateMap<StockItem, StockItemDTO>();
            });
        }
    }
}
