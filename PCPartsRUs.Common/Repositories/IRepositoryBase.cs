﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace PCPartsRUs.Common.Repositories
{
    public interface IRepositoryBase<T> where T : class
    {
        T GetById(long Id);
        IEnumerable<T> GetAll();

        IEnumerable<T> Search(Expression<Func<T, bool>> predicate);

        void Add(T entity);
        void AddRange(IEnumerable<T> entities);

        void Remove(T entity);
        void RemoveRange(IEnumerable<T> entities);
    }
}
