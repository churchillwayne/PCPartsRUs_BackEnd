﻿using PCPartsRUs.Common.Models;

namespace PCPartsRUs.Common.Repositories
{
    public interface IPartRepository : IRepositoryBase<Part>
    {
    }
}

