﻿using PCPartsRUs.Common.Models;

namespace PCPartsRUs.Common.Repositories
{
    public interface IStockItemRepository : IRepositoryBase<StockItem>
    {
    }
}
