﻿using PCPartsRUs.Common.Repositories;
using System;

namespace PCPartsRUs.Common.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        IPartRepository Parts { get; }
        IStockItemRepository Stock { get; }

        void Complete();
    }
}
