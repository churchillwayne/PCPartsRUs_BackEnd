﻿using System;

namespace PCPartsRUs.Common.DTOs
{
    public class StockItemDTO
    {
        public long Id { get; set; }
        public string SerialNumber { get; set; }
        public int PartId { get; set; }
        public PartDTO Part { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}
