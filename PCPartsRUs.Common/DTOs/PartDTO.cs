﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCPartsRUs.Common.DTOs
{
    public class PartDTO
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public IEnumerable<StockItemDTO> StockItems { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}
