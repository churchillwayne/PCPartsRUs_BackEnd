﻿using PCPartsRUs.Common.DTOs;

namespace PCPartsRUs.Common.Managers
{
    public interface IPartManager
    {
        PartDTO GetPartByID(int Id);
    }
}
